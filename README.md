# CS631_Project

## Code
See file https://git.uwaterloo.ca/y326zhen/cs631_project/-/blob/d59746a6b76f119b1958cbbf93bd6e60e3775f54/CS631_Project.ipynb

## Overleaf links
https://www.overleaf.com/1675258949zpqvshwxrcsm (Edit)

https://www.overleaf.com/read/jpyxtycbsvhz (View)

## Report structure

Introduction
- spark 
	- RDD (parallel computing)
- project overview
- dataset
	- overview
	- source 

Methods
- Word embedding method
	- Bag of words(Word Counts)
	- TF-IDF
	
- Model
	- KNN
		- formula
		- citations
		- related works (similar used in previous papers.)
	- Naive Bayes
		- formula
		- citations
		- related works (similar used in previous papers.)

Results
- KNN and Naive Bayes model test accuracies

Discussion
- Naive Bayes future works
- K-NN model limitations

Conclusion

